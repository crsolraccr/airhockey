#include "ros/ros.h"
#include "vision_estimator.hpp"

int main( int argc, char** argv )
{

  ros::init(argc, argv, "pepe");
  VisionEstimator v_estimator1(1);
  VisionEstimator v_estimator2(2);
  v_estimator1.setup();
  v_estimator2.setup();
  
  ros::Rate rate(30);
  while (ros::ok())
  {
    ros::spinOnce();
    v_estimator1.run();
    v_estimator2.run();
    rate.sleep();
  }
}