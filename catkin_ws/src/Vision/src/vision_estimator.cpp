#include "vision_estimator.hpp"

#define LOG(x) std::cout<<x<<std::endl
VisionEstimator::VisionEstimator(int n_robot):diskDetector_(false),n_robot_(n_robot){


	image_sub_ = nh_.subscribe("/camera",1,&VisionEstimator::imageCallback,this);
	
	std::string robot_name;
	if (n_robot_ == 1) robot_name = "/robot1/";
	else if (n_robot_ == 2) robot_name = "/robot2/";
	std::string topic_name = robot_name + "disk";

	std::cout << "Disk pose publisher at: " << topic_name << std::endl;
	pose_pub_  = nh_.advertise<geometry_msgs::PoseStamped>(topic_name,1);

}

VisionEstimator::~VisionEstimator(){
}

void VisionEstimator::setup(){

}

void VisionEstimator::publishDiskPose(){
	geometry_msgs::PoseStamped real_pose;
	// TODO improve this 
	auto value_x= (center_.x - 235)/(416.0f-235.0f) * 7.39 ;
	auto value_y= center_.y * (-14.88f/(429.0f-59.0f))  + 14.88+ 2.3;
	if (n_robot_ == 1 ){
	real_pose.pose.position.x = value_x;  
	real_pose.pose.position.y = value_y;  
	}
	else if (n_robot_== 2){
		real_pose.pose.position.x = MAX_X - value_x;  
		real_pose.pose.position.y = MAX_Y - value_y;  
	}
	// clip values x
	if (real_pose.pose.position.x > MAX_X)  real_pose.pose.position.x= MAX_X;  
	else if (real_pose.pose.position.x < 0) real_pose.pose.position.x= 0;
	
	// clip values y
	if (real_pose.pose.position.y > MAX_Y)  real_pose.pose.position.y= MAX_Y;  
	else if (real_pose.pose.position.y < 0) real_pose.pose.position.y= 0;
	
	pose_pub_.publish(real_pose);

}

void VisionEstimator::run(){


	// std::cout << "x:" << center_.x << std::endl;
	// std::cout << "y:" << center_.y << std::endl;
	publishDiskPose();
}



void VisionEstimator::imageCallback(const sensor_msgs::CompressedImage& msg)
{
	// std::cout << "new image"<<std::endl;
	try
	{
		image_ = cv::imdecode(cv::Mat(msg.data),1);//convert compressed image data to cv::Mat

		if(!image_.empty()){
			if (diskDetector_.process_frame(image_) > 0 ) // sucess
			center_ = diskDetector_.getCenter();
		}

	}
	catch (cv::Exception& e)
	{
		ROS_ERROR("Could not convert to image!");
	}
}
