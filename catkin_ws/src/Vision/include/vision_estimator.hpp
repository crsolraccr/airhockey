#ifndef __VISION_ESTIMATOR_H_
#define __VISION_ESTIMATOR_H_

#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "sensor_msgs/CompressedImage.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"

#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <string>

#include "disk_detector.hpp"

// TODO IMPROVE THIS VALUES
#define MAX_X 7.4f
#define MAX_Y 15.134f

class VisionEstimator{


private:
    ros::NodeHandle nh_;
    ros::Subscriber image_sub_;
    ros::Publisher  pose_pub_;
    
    DiskDetector diskDetector_;
    cv::Mat image_;
    cv::Point2f center_;
    int n_robot_;
public:

    VisionEstimator(int n_robot);
    ~VisionEstimator();
    
    void showImage(std::string name, const cv::Mat& im );
    void setup();
    void run();

private:
    cv::Mat maskImage(const cv::Mat& im);
    void imageCallback(const sensor_msgs::CompressedImage& msg);
    void publishDiskPose();

};
#endif