#include "robotController.hpp"


int main( int argc, char** argv )
{
  ros::init(argc, argv, "control_robot2");
  // robotController Controller1("/robot1/");
  robotController Controller2("/robot2/");

  ros::Rate r(50);


  while (ros::ok())
  {
    // Controller1.Defend();
    Controller2.Play();
    ros::spinOnce();
    r.sleep();
  }
}