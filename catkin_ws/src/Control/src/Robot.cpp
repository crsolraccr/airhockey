#include "Robot.hpp"

void Robot::updateVelocity(const geometry_msgs::PoseStamped& new_pose){
    double delta_time = (new_pose.header.stamp.toNSec()-time)/std::pow(10,9);
    if (delta_time < 0) delta_time += 1;
    time = new_pose.header.stamp.toNSec();
    velocity.linear.x = (new_pose.pose.position.x-pose.pose.position.x)/delta_time;
    velocity.linear.y = (new_pose.pose.position.y-pose.pose.position.y)/delta_time;
}

void Robot::moveTo(double pos[2], float kp[2], float kv[2]){
    double ep[2];
    ep[0] = pos[0] - pose.pose.position.x;
    ep[1] = pos[1] - pose.pose.position.y;
    double vx = ep[0]*kp[0] - (ep_past[0]-ep[0])*kv[0];
    double vy = ep[1]*kp[1] - (ep_past[1]-ep[1])*kv[1];
    ep_past[0] = ep[0];
    ep_past[1] = ep[1];
    setSpeed(vx, vy); 
    //std::cout << "Disk:  x = " << pos[0] << " y = " << pos[1] << std::endl;
    //std::cout << "Robot:  x = " << pose.pose.position.x << " y = " << pose.pose.position.y << std::endl;
}

void Robot::setSpeed(double vx, double vy){
    geometry_msgs::Twist vel_command;
    double v_max;
    if(abs(vx)>abs(vy))v_max = vx;
    else v_max = vy;
    if(v_max>MAX_SPEED){
        vx *= MAX_SPEED/v_max;
        vy *= MAX_SPEED/v_max;
    }
    vel_command.linear.y = -vx;
    vel_command.linear.z = vy;
    //std::cout << "Speed:  x = " << vx << " y = " << vy << std::endl;
    twist_pub.publish(vel_command);
}

void Robot::stop(){
    setSpeed(0,0);
}