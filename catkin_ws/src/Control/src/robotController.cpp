#include "../include/robotController.hpp"

void robotController::Play(){
    double DiskPos[2], DiskVel[2], dist[2];

    disk.getPose(DiskPos);
    disk.getVelocity(DiskVel);

    if(DiskPos[1]>trigger_y){
        if(DiskVel[1]<0) Defend();
        else Defend(defense_position);
    }
    else{
        if(disk.checkLastTraj(defense_position[1])){
            double estimated_position[2] = {disk.getTrajectoryIntersection(2), 2};
            getDist(dist, estimated_position);
            if((dist[0] < 0.6 ) || (DiskVel[1] >= -slow)){
                if(DiskVel[1]>0 && (getDist()<0.8)){
                    Attack(500);
                }
                else Attack();
            }
            else Defend();
        }
        else{
            if(disk.getVelocity() < slow || (DiskVel[1] >= -slow)){
                if(DiskVel[1]>0 && (getDist()<0.8)){
                    Attack(500);
                }
                Attack();
            }
            else{
                Defend();
            }
        }
    }
    /*if(DiskPos[1]>6){
        Defend();
    }
    else{
        robot.stop();
    }*/
}

void robotController::Defend(){
    double DiskPos[2];
    float kp[2], kv[2];
    double Pos[2] = {disk.getTrajectoryIntersection(2), 2};

    kp[0] = 6;
    kp[1] = 6;
    kv[0] = 0;
    kv[1] = 0;

    robot.moveTo(Pos, kp, kv);
}

void robotController::Defend(double position[2]){
    double DiskPos[2];
    float kp[2], kv[2];

    kp[0] = 6;
    kp[1] = 6;
    kv[0] = 0;
    kv[1] = 0;

    robot.moveTo(position, kp, kv);
}

void robotController::Attack(){
    double DiskPos[2];
    disk.getPose(DiskPos);
    float kp[2], kv[2];
    double Pos[2] = {DiskPos[0], DiskPos[1]};


    kp[0] = 5;
    kp[1] = 5;
    kv[0] = 0;
    kv[1] = 0;

    robot.moveTo(Pos, kp, kv);
}

void robotController::Attack(float k){
    double DiskPos[2];
    disk.getPose(DiskPos);
    double Pos[2] = {DiskPos[0], DiskPos[1]};
    float kp[2], kv[2];

    kp[0] = k;
    kp[1] = k;
    kv[0] = 0;
    kv[1] = 0;

    robot.moveTo(Pos, kp, kv);
}

double robotController::getDist(){
    double robotPos[2], diskPos[2];
    robot.getPose(robotPos);
    disk.getPose(diskPos);
    return std::sqrt(std::pow((diskPos[0]-robotPos[0]),2)+std::pow((diskPos[1]-robotPos[1]),2));
}

void robotController::getDist(double dist[2]){
    double robotPos[2], diskPos[2];
    robot.getPose(robotPos);
    disk.getPose(diskPos);
    dist[0] = (diskPos[0]-robotPos[0]);
    dist[1] = (diskPos[1]-robotPos[1]);
}

void robotController::getDist(double dist[2], double pos[2]){
    double robotPos[2];
    robot.getPose(robotPos);
    dist[0] = (pos[0]-robotPos[0]);
    dist[1] = (pos[1]-robotPos[1]);
}
