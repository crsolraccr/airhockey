#include "Disk.hpp"

void Disk::updateVelocity(const geometry_msgs::PoseStamped& new_pose){
    double delta_time = ros::Time::now().toNSec()-time;
    time = ros::Time::now().toNSec();
    velocity.linear.x = ((new_pose.pose.position.x-pose.pose.position.x)*std::pow(10,9))/delta_time;
    velocity.linear.y = ((new_pose.pose.position.y-pose.pose.position.y)*std::pow(10,9))/delta_time;
    //std::cout << "velocity x = " << velocity.linear.x << std::endl;
    //std::cout << "velocity y = " << velocity.linear.y << std::endl;
    #ifdef DEBUG
    std::cout << "delta time = " << time << std::endl;
    std::cout << "velocity x = " << velocity.linear.x << std::endl;
    std::cout << "velocity y = " << velocity.linear.y << std::endl;
    #endif
    if((time - compute_traj_time)>TRAJ_FREQ){
        compute_traj_time = time;
        computeTrajectory();
    }
}

void Disk::computeTrajectory(){
    double DiskVel[2];
    double inc_y, inc_x;
    float local_bound;

    keyPoint previous_traj_point = actual_traj.KeyPoints[0];

    getPose(actual_traj.KeyPoints[0].pos);
    getVelocity(DiskVel);

    actual_traj.KeyPoints[0].ang = atan2(DiskVel[0],DiskVel[1]);
    #ifdef DEBUG
    std::cout << "x: " <<  actual_traj.KeyPoints[0].pos[0] << "   y: " << actual_traj.KeyPoints[0].pos[1] << "  ang: " << actual_traj.KeyPoints[0].ang << std::endl;
    std::cout << "ang  " << actual_traj.KeyPoints[0].ang << std::endl;
    #endif

    for(int i = 1; i < NUM_BOUNCES; i++){
        if(actual_traj.KeyPoints[i-1].ang > 0) local_bound = RIGHT_BOUND;
        else local_bound = LEFT_BOUND;

        inc_x = (local_bound-actual_traj.KeyPoints[i-1].pos[0]);
        inc_y = inc_x/tan(actual_traj.KeyPoints[i-1].ang);
        if (checkBounds(actual_traj.KeyPoints[i-1].pos[1] + inc_y)){
            actual_traj.KeyPoints[i].pos[1] = actual_traj.KeyPoints[i-1].pos[1] + inc_y;
            actual_traj.KeyPoints[i].pos[0] = local_bound;
            if(abs(actual_traj.KeyPoints[i-1].ang) < M_PI/2)
                actual_traj.KeyPoints[i].ang = -actual_traj.KeyPoints[i-1].ang + actual_traj.KeyPoints[i-1].ang/2;
            else{
                if(actual_traj.KeyPoints[i-1].ang>0)
                    actual_traj.KeyPoints[i].ang = -actual_traj.KeyPoints[i-1].ang - (M_PI - actual_traj.KeyPoints[i-1].ang)/2;
                else
                    actual_traj.KeyPoints[i].ang = -actual_traj.KeyPoints[i-1].ang + (M_PI + actual_traj.KeyPoints[i-1].ang)/2;
            }

            
        }
        else{
            if(abs(actual_traj.KeyPoints[i-1].ang) > M_PI/2) local_bound = BACKWARD_BOUND;
            else local_bound = FORWARD_BOUND;

            inc_x = tan(actual_traj.KeyPoints[i-1].ang)*(local_bound-actual_traj.KeyPoints[i-1].pos[1]);
            actual_traj.KeyPoints[i].pos[0] = actual_traj.KeyPoints[i-1].pos[0] + inc_x;
            actual_traj.KeyPoints[i].pos[1] = local_bound;

            actual_traj.KeyPoints[i].ang = M_PI - actual_traj.KeyPoints[i-1].ang;
            if (actual_traj.KeyPoints[i].ang > M_PI) actual_traj.KeyPoints[i].ang-=2*M_PI;
        }
        #ifdef DEBUG
        std::cout << "x: " <<  actual_traj.KeyPoints[i].pos[0] << "   y: " << actual_traj.KeyPoints[i].pos[1] << "  ang: " << actual_traj.KeyPoints[i].ang << std::endl;
        #endif
    }
    #ifdef DEBUG
    std::cout << "FIN" << std::endl;
    #endif

    if(checkBounds()){
        if(!guessed){
            //std::cout << "PEPE" << std::endl;
            if((actual_traj.KeyPoints[0].ang*previous_traj_point.ang)<0) {
                //std::cout << "AAAAAAAAAAAAAAAAAAAAA" << std::endl;
                //for(int j=0;j<NUM_BOUNCES;j++){
                //    std::cout << "x: " <<  actual_traj.KeyPoints[j].pos[0] << "   y: " << actual_traj.KeyPoints[j].pos[1] << "  ang: " << actual_traj.KeyPoints[j].ang << std::endl;
                //}
                guess_k_angle(previous_traj_point.ang);
            }
        }
    }

    previous_velocity = velocity;
}

bool Disk::checkBounds(double y){
    if ((y < FORWARD_BOUND) && (y > BACKWARD_BOUND)) return true;
    else return false;
}

bool Disk::checkBounds(){
    double pos[2] = {actual_traj.KeyPoints[0].pos[0], actual_traj.KeyPoints[0].pos[1]};
    if (pos[1] > 14.5 || pos[1] < 0.75 || pos[0] > 6.6 || pos[0] < 0.75){
        if(pos[0]!=0||pos[1]!=0) {
            return true;
        }
        else return false;
    }
    else return false;
}

bool Disk::checkLastTraj(double y){
    return actual_traj.KeyPoints[1].pos[1] < y;
}

double Disk::getTrajectoryIntersection(double y){
    bool found_flag;
    int i;
    if (actual_traj.KeyPoints[0].pos[1] > y){
        for(i = 1; i < NUM_BOUNCES; i++){
            if(actual_traj.KeyPoints[i].pos[1]<y){
                found_flag = true;
                break;
            }
        }
        if(found_flag){
            float y_prop = (actual_traj.KeyPoints[i-1].pos[1] - y)/(actual_traj.KeyPoints[i-1].pos[1] - actual_traj.KeyPoints[i].pos[1]);
            #ifdef DEBUG
            std::cout << "velocidad = " <<  std::sqrt(std::pow(velocity.linear.x,2)+std::pow(velocity.linear.y,2)) << std::endl;
            std::cout << "i: "<< i << std::endl;//" x[i] = " <<actual_traj.KeyPoints[i].pos[0] << " x[i-] = " <<actual_traj.KeyPoints[i-1].pos[0] << std::endl;
            // std::cout << "inc x: "<< (actual_traj.KeyPoints[i-1].pos[0] - actual_traj.KeyPoints[i].pos[0]) << std::endl;
            // std::cout << "y_prop: "<< y_prop << std::endl;
            // std::cout << "Total: "<< y_prop*(actual_traj.KeyPoints[i-1].pos[0] - actual_traj.KeyPoints[i].pos[0]) << std::endl;
            #endif
            return  actual_traj.KeyPoints[i-1].pos[0] - y_prop*(actual_traj.KeyPoints[i-1].pos[0] - actual_traj.KeyPoints[i].pos[0]);
        }
    }
    return -1.0;
}

float Disk::guess_k_angle(float ang){
    #ifdef DEBUG
    std::cout << "FIRST BOUNCE!!!!!!!!!!!!!!" << guessed << std::endl;
    std::cout << "ang  " << ang << std::endl;
    std::cout << "ang  " << actual_traj.KeyPoints[0].ang << std::endl;
    std::cout << "velocity x = " << velocity.linear.x << std::endl;
    std::cout << "velocity y = " << velocity.linear.y << std::endl;
    std::cout << "VELOCITY = " << std::sqrt(std::pow(velocity.linear.x,2)+std::pow(velocity.linear.y,2)) << std::endl;
    #endif
    guessed = true;
}