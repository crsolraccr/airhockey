#include "../include/GameObject.hpp"

void GameObject::poseCallback(const geometry_msgs::PoseStamped& msg){
    updateVelocity(msg);
    pose = msg;
}

void GameObject::getPose(double pos[2]){
    pos[0] = pose.pose.position.x;
    pos[1] = pose.pose.position.y;
}

void GameObject::getVelocity(double vel[2]){
    vel[0] = velocity.linear.x;
    vel[1] = velocity.linear.y;
}

double GameObject::getVelocity(){
    // std::cout << "velocity x = " << velocity.linear.x << std::endl;
    // std::cout << "velocity y = " << velocity.linear.y << std::endl;
    // std::cout << "VELOCITY = " << std::sqrt(std::pow(velocity.linear.x,2)+std::pow(velocity.linear.y,2)) << std::endl;
    return std::sqrt(std::pow(velocity.linear.x,2)+std::pow(velocity.linear.y,2));
}