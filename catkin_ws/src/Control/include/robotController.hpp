#ifndef ROBOT
#define ROBOT
#include "Robot.hpp"
#endif

#ifndef DISK
#define DISK
#include "Disk.hpp"
#endif

#ifndef ROS
#define ROS
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#endif

#ifndef STD
#define STD
#include <string>
#include <iostream>
#endif

class robotController{
    private:
        Robot robot;
        Disk disk;
        float trigger_y = 7;
        float slow = 5;
        double defense_position[2] = {3.5, 1};

    public:
        robotController(std::string robot_name)
            :robot(robot_name),disk(robot_name+"disk"){};
        void Play();
        void Defend();
        void Defend(double position[2]);
        void Attack();
        void Attack(float k);
        double getDist();
        void getDist(double dist[2]);
        void getDist(double dist[2], double pos[2]);
};
