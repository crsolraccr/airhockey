#ifndef GAMEOBJECT
#define GAMEOBJECT
#include "GameObject.hpp"
#endif

#ifndef DISK
#define DISK
#include "Disk.hpp"
#endif

// #define MAX_SPEED 20

#include<string>

class Robot:public GameObject{
    private:
        double ep_past[2] = {0, 0};
        ros::Publisher twist_pub;

    public:
        Robot(std::string name):GameObject(name+"pose"){
            auto twist_pub_topic = name+"control";
            std::cout << "twist_pub_name : "<<twist_pub_topic<<std::endl;
            twist_pub = n.advertise<geometry_msgs::Twist>(twist_pub_topic, 1);
        }
        void updateVelocity(const geometry_msgs::PoseStamped& pose);
        void moveTo(double pos[], float kp[], float kv[]);
        void followDisk(Disk disk);
        void setSpeed(double vx, double vy);
        void stop();
};