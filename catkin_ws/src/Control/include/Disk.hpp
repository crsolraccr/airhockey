#ifndef GAMEOBJECT
#define GAMEOBJECT
#include "GameObject.hpp"
#endif

#ifndef MATH
#define MATH
#include <math.h>
#endif

#define INF 0.00000001
#define NUM_BOUNCES 5
#define FORWARD_BOUND 14.9
#define BACKWARD_BOUND 0.35
#define LEFT_BOUND 0.35
#define RIGHT_BOUND 7

#define TRAJ_FREQ 0.01
#define MAX_SPEED 30

struct keyPoint{
    double pos[2];
    float ang;
};

struct trajectory{
    keyPoint KeyPoints[NUM_BOUNCES];
    float boundaries[4]={FORWARD_BOUND,BACKWARD_BOUND,LEFT_BOUND,RIGHT_BOUND};
};

class Disk:public GameObject{
    private:
        trajectory actual_traj;
        geometry_msgs::Twist previous_velocity;
        double compute_traj_time;
        bool guessed = false;

    public:
        using GameObject::GameObject;
        void updateVelocity(const geometry_msgs::PoseStamped& pose);
        void computeTrajectory();
        double getTrajectoryIntersection(double y);
        bool checkBounds(double y);
        bool checkBounds();
        bool checkLastTraj(double y);
        float guess_k_angle(float ang);
};