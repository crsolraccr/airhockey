#ifndef ROS
#define ROS
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#endif

#ifndef STD
#define STD
#include <string>
#include <iostream>
#endif

//#define DEBUG

class GameObject{
    protected:
        ros::NodeHandle n;
        geometry_msgs::PoseStamped pose;
        geometry_msgs::Twist velocity;
        double time;

    private:
        ros::Subscriber pose_sub;

    public:
        GameObject(std::string topic_name){
            std::cout << "new GameObject subscribed to : " << topic_name <<std::endl; 
            pose_sub = n.subscribe(topic_name,1,&GameObject::poseCallback, this);
        }
        void poseCallback(const geometry_msgs::PoseStamped& msg);
        virtual void updateVelocity(const geometry_msgs::PoseStamped& pose){};
        void getVelocity(double vel[2]);
        double getVelocity();
        void getPose(double pos[2]);
};
