# README #

Aquí se muestran los pasos para la instalación de los paquetes de ROS, para ver una guia más detallada de como lanzar el simulador veáse el apéndice B de la memoria entregada.
### Para compilar los paquetes ###
* $cd catkin_ws
* $catkin build
* $source devel/setup.bash

### Para ejecutar los nodos ###
* $rosrun Control main 
* $rosrun Vision Vision_main

